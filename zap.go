package wzap

import (
	"os"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

var logger *zap.SugaredLogger

func init() {
	cores, _ := getLogCore()
	z := zap.New(zapcore.NewTee(cores...))
	logger = z.Sugar()
}
func getEncoder() zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = func(t time.Time, encoder zapcore.PrimitiveArrayEncoder) {
		encoder.AppendString(t.Format("2006-01-02 15:04:05"))
	}
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	return zapcore.NewConsoleEncoder(encoderConfig)
}

func getLogCore() ([]zapcore.Core, zapcore.Encoder) {
	encoder := getEncoder()
	//日志级别
	highPriority := zap.LevelEnablerFunc(func(lev zapcore.Level) bool { //error级别
		return lev >= zap.WarnLevel
	})
	lowPriority := zap.LevelEnablerFunc(func(lev zapcore.Level) bool { //info和debug级别,debug级别是最低的
		return lev < zap.WarnLevel && lev >= zap.DebugLevel
	})

	//info文件writeSyncer
	infoFileWriteSyncer := zapcore.AddSync(&lumberjack.Logger{
		Filename:   "logs/current.info.log", //日志文件存放目录，如果文件夹不存在会自动创建
		MaxSize:    10,                      //文件大小限制,单位MB
		MaxBackups: 10,                      //最大保留日志文件数量
		MaxAge:     30,                      //日志文件保留天数
		Compress:   false,                   //是否压缩处理
		LocalTime:  true,
	})
	infoFileCore := zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(infoFileWriteSyncer, zapcore.AddSync(os.Stdout)), lowPriority) //第三个及之后的参数为写入文件的日志级别,ErrorLevel模式只记录error级别的日志
	//error文件writeSyncer
	errorFileWriteSyncer := zapcore.AddSync(&lumberjack.Logger{
		Filename:   "logs/current.error.log", //日志文件存放目录
		MaxSize:    10,                       //文件大小限制,单位MB
		MaxBackups: 10,                       //最大保留日志文件数量
		MaxAge:     30,                       //日志文件保留天数
		Compress:   false,                    //是否压缩处理
		LocalTime:  true,
	})
	errorFileCore := zapcore.NewCore(encoder, zapcore.NewMultiWriteSyncer(errorFileWriteSyncer, zapcore.AddSync(os.Stdout)), highPriority) //第三个及之后的参数为写入文件的日志级别,ErrorLevel模式只记录error级别的日志

	return []zapcore.Core{infoFileCore, errorFileCore}, encoder
}
