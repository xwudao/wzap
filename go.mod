module gitee.com/xwudao/wzap

go 1.16

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/pkg/errors v0.9.1
	go.uber.org/zap v1.17.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
